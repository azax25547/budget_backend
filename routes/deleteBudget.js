const deleteBudget = require("express").Router();
const data = require("../data");

deleteBudget.delete("/delete/:id", (req, res) => {
    const { id } = req.params;
    if(!data.envelopes.some((ctg) => ctg.id === id)){
        return res.status(400).json({
            message: "Unable to Get the Record!, Input correct ID",
            done: false
        })
    }
    data.envelopes = data.envelopes.filter((x) => x.id !== id)
    return res.status(202).json({
        message: "Budget deleted Successfully",
        done: true
    })

})

module.exports = {
    deleteBudget
}