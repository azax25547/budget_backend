const updateBudget = require("express").Router();
const data = require("../data");
const {updateInPlace} = require("../utils/updateinPlace");


updateBudget.put("/update/:id", (req, res) => {
    const { id } = req.params;
    if(!data.envelopes.some((ctg) => ctg.id === id)){
        return res.status(400).json({
            message: "Unable to Get the Record!, Input correct ID",
            done: false
        })
    }
    // const state = data.envelopes;
    data.envelopes = updateInPlace(id, req.body);
    return res.status(202).json({
        message: "Data Updated Successfully",
        done: true
    })


})

module.exports = {
    updateBudget
}