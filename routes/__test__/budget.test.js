const request = require("supertest")
const app = require("../../app")


describe("GET /status", () => {
    it('should return 200 on successful status', function () {
        return request(app)
            .get("/status")
            .expect(200)
    });
})

describe("Budget APIs", () => {

    describe("POST /api/create/category", () => {
        it('should create a new category and return status 201', function () {
            return request(app).post('/app/create/category').send({ category:"Category" }).expect(201).expect({
                "message": "New Category created Successfully!",
                "done": true
            })
        });
    })
})