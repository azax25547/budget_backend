const {updateBudget} = require("./updateBudget");
const {deleteBudget} = require("./deleteBudget");
const {getAll} = require("./getAll");
const {getSingle} = require("./getSingleBudget");
const {create} = require("./createBudget");
const {transferBudget} = require("./transferBudget");
const {setBudgetAmount} = require("./setBudgetAmount");
const {createCategory} = require("./createCategory");
const {getCategory} = require("./getCategory");
const {getBudget} = require("./getBudget");

module.exports.routes = {
    create,
    getSingle,
    getAll,
    deleteBudget,
    updateBudget,
    transferBudget,
    setBudgetAmount,
    createCategory,
    getCategory,
    getBudget
}