const getCategory = require('express').Router();
const  data  = require("../data");

getCategory.get('/category', (req, res) => {

    return res.status(200).json({
        data: data.categories
    })

})

module.exports = {
    getCategory
}