const getAll = require('express').Router();
const  data  = require("../data");

getAll.get('/get/', (req, res) => {

    return res.status(200).json({
        data: data.envelopes
    })

})

module.exports = {
    getAll
}