const transferBudget = require("express").Router();
const data = require("../data");
const {transferBudgets} = require("../utils/transfer");

transferBudget.post("/transfer", (req, res) => {
    let { from, to, amount } = req.body;

    amount = parseInt(amount, 10);
    from  = from.toString();
    to = to.toString();

    if (!from || !to || !amount)
        return res.json({
            message: "Invalid Input Arguments",
            done: false
        }).status(400)


    if(!data.envelopes.some((ctg) => ctg.category === from || ctg.category === to)){
        return res.status(400).json({
            message: "Unable to Get the Record!, Input correct ID", done: false
        })
    }

    data.envelopes = transferBudgets(from, to, amount);
    return res.status(201).json({
        message: "Data Updated Successfully", done: true
    })


})

module.exports = {
    transferBudget
}