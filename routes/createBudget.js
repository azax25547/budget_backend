const create = require('express').Router();
const  data  = require("../data");
const { _ } = require("lodash");

create.post('/create', (req, res) => {
    let { category,amount } = req.body;
    amount = parseInt(amount,10);

    let leftAmount = data.total;

    if (!category || !amount)
        return res.json({
            message: "Invalid Input Arguments",
            done: false
        }).status(400)

    if(leftAmount === 0)
        return res.status(400).json({message: "You have used all of your Total Budgets", done: false})

    if(amount > leftAmount)
        return res.status(400).json({message: "Amount must be Less that the Total Amount", done: false})


    if(!data.categories.includes(category))
        data.categories.push(category);


    if(!data.envelopes.some((ctg) => ctg.category === category)) {
        data.envelopes.push({
            id: _.uniqueId(),
            category,
            amount
        })
        data.total = leftAmount - amount;
    }else {
        return res.status(400).json({
            done:false,
            message: "This Category is already added"
        })
    }

    res.status(201).json({
        done: true,
        message: "Budget Created Successfully!"
    });

})


module.exports = {
    create
}