const getSingle = require('express').Router();
const  data  = require("../data");

getSingle.get('/get/:id', (req, res) => {
    let { id } = req.params;
    if(!data.envelopes.some((ctg) => ctg.id === id)) {
        return res.status(400).json({
            message: "Unable to Get the Record!, Input correct ID"
        })
    }
    return res.status(200).json({
        data: data.envelopes.find((x) => x.id === id),
    })

})

module.exports = {
    getSingle
}