const setBudgetAmount = require('express').Router();
const  data  = require("../data");

setBudgetAmount.post('/set', (req, res) => {
    const { amount } = req.body;
    data.total = parseInt(amount, 10);

    return res.status(201).json({
        message:"budget Amount set Successfully!",
        done: true
    })

})

module.exports = {
    setBudgetAmount
}