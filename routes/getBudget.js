const getBudget = require('express').Router();
const  data  = require("../data");

getBudget.get('/budget', (req, res) => {

    return res.status(200).json({
        data: data.total
    })

})

module.exports = {
    getBudget
}