const createCategory = require('express').Router();
const  data  = require("../data");

createCategory.post('/create/category', (req, res) => {
    const { category } = req.body;
    if(data.categories.includes(category))
        return res.status(400).json({
            message:"Category is already Present",
            done: true
        })
    data.categories.push(category);

    return res.status(201).json({
        message:"New Category created Successfully!",
        done: true
    })

})

module.exports = {
    createCategory
}