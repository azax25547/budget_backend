const {json} = require("express");
const { routes } = require("./routes/");
const app = require("express")();
const morgan = require("morgan")
const cors = require('cors');
app.use(json())

app.use(cors())
app.use(morgan("common"));

app.get("/status",(req, res) => {
    res.json({
        message: "Up"
    }).status(200);
})


app.use("/api", routes.create);
app.use("/api", routes.deleteBudget);
app.use("/api", routes.getSingle);
app.use("/api", routes.getAll);
app.use("/api", routes.updateBudget);
app.use("/api", routes.transferBudget);
app.use("/api", routes.getCategory);
app.use("/api", routes.getBudget);

app.use("/api", routes.createCategory);
app.use("/api", routes.setBudgetAmount);

module.exports = app;