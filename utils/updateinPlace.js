const data = require("../data");
module.exports = {
    updateInPlace(id, payload) {
        return data.envelopes.map((obj) => {
            return obj.id === id ? { ...obj, ...payload } : obj
        })
    }
}