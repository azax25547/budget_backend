const data = require("../data");
const { _ } = require("lodash");

module.exports = {
    transferBudgets(from, to, transferAmount) {
        const fromd =  data.envelopes.map((obj) => {
            return obj.category === from ? { ...obj, amount: obj.amount - transferAmount } : obj
        })

        return fromd.map((obj) => {
            return obj.category === to ? {...obj, amount: obj.amount + transferAmount} : obj
        })
    }
}